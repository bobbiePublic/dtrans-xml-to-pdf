<?php

const DTRANS_PDF_STANDALONE_TEST = TRUE;

require_once __DIR__ . '/../config.php';
use DtransXML2PDF\PDFBuilder;

const VIEW_KEY = '@@YOUR_VIEW_KEY_HERE@@';

$builder = new PDFBuilder(VIEW_KEY);
$pdf = $builder->create_pdf();

file_put_contents('bin/demo.pdf', $pdf);