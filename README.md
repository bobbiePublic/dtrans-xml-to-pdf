# dtrans-xml-to-pdf
Requires chrome/chromium executable.

## What is this?
This is a extension module for the dtrans-standalone which renders ubl xml into a human-readable pdf generation by using
the frontend. By default, the xml used is embed into the PDF as PDF/A-3b.

## How does it work?
It renderers the xml accessed by a sharing key in the frontend into a headless Chrome browser and prints the result
into a PDF file.

## How can I get it to work?
Please have a look at the config.php and ensure all paths are correct and accessible. Especially the temp file
which is used to share a single chrome instance between threads to reduce memory usage can be tricky on a non Linux machine.
