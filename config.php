<?php


if(!defined('DTRANS_PDF_STANDALONE_TEST')) {
    require __DIR__ . '/../../autoload.php';
    define("PATH_LOG_FILES", __DIR__ . '/../../../logs/');
} else {
    require __DIR__ . '/vendor/autoload.php';
    define("PATH_LOG_FILES", __DIR__ . '/etc/bin/');
}

define("PATH_TEMP_FILE", sys_get_temp_dir() . '/' . 'dtransXMLtoPDF-chrome-socket');

\DtransXML2PDF\PDFLogger::_initialize();
\DtransXML2PDF\PDFConfig::load_configs();