<?php

namespace DtransXML2PDF;

abstract class PDFConfig
{
    const CFG_NAME = 'config';

    private static array $configuration = array();

    public static function load_configs()
    {
        if (!empty(self::$configuration)) return;
        self::load_config(self::CFG_NAME);
    }

    const CONFIG_FILE_EXTENSION = '.ini';

    private static function load_config(string $filename): void
    {
        $dir = __DIR__ . '/../';
        $file = parse_ini_file($dir . $filename . self::CONFIG_FILE_EXTENSION, true, INI_SCANNER_NORMAL);
        if ($file === false) {
            PDFLogger::info('Failed to load config file', ['filename' => $filename . self::CONFIG_FILE_EXTENSION]);
            self::$configuration[$filename] = array();
            return;
        }

        self::$configuration[$filename] = $file;
    }

    // if section is set to null this property will be searched in global scope
    // internal get function used by all external get functions
    private static function _get(string $file, ?string $section, string $property) /*?string|?array*/
    {
        $exits = array_key_exists($file, self::$configuration);
        if (!$exits) {
            PDFLogger::warning('Configuration file is not loaded.', ['file' => $file, 'section' => $section, 'property' => $property]);
            return null;
        }

        // file is loaded. check if section exists
        $f = self::$configuration[$file];
        if (!is_null($section))
            $f = array_key_exists($section, $f) ? $f[$section] : null;

        // check if property exists
        if (!is_null($f) && array_key_exists($property, $f))
            return $f[$property];

        PDFLogger::info('Property not found in configuration file.', ['file' => self::CFG_NAME, 'section' => $section, 'property' => $property]);
        return null;
    }

    public static function get_array(?string $section = null, ?array $default = null): ?array
    {
        if (!array_key_exists(self::CFG_NAME, self::$configuration)) {
            PDFLogger::warning('Configuration file is not loaded.', ['file' => self::CFG_NAME, 'section' => $section]);
            return $default;
        }

        $f = self::$configuration[self::CFG_NAME];
        if (is_null($section))
            return $f;
        else
            return array_key_exists($section, $f) ? $f[$section] : $default;
    }

    public static function get_string(?string $section, string $property, ?string $default = null): ?string
    {
        $value = self::_get(self::CFG_NAME, $section, $property);
        return is_null($value) || strcmp('null', strtolower($value)) === 0 ? $default : $value;
    }

    public static function get_int(?string $section, string $property, ?int $default = null): ?int
    {
        $value = self::_get(self::CFG_NAME, $section, $property);
        return is_numeric($value) ? (int)$value : $default;
    }

    // technically this is a float too as in PHP float/double primitives are stored in platform dependent C double type
    // this is why there is no need for a get_float function
    public static function get_double(?string $section, string $property, ?float $default = null): ?float
    {
        $value = self::_get(self::CFG_NAME, $section, $property);
        return is_numeric($value) ? doubleval($value) : $default;
    }

    public static function get_bool(?string $section, string $property, ?bool $default = null): ?bool
    {
        $value = self::_get(self::CFG_NAME, $section, $property);
        if (is_null($value)) return $default;
        return !(empty($value) || $value == 0 || strcmp(strtolower($value), 'no') === 0 || strcmp(strtolower($value), 'false') === 0);
    }
}