<?php

namespace DtransXML2PDF;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

abstract class PDFTools {
    public static function download_xml(string $view_key, string $frontend_uri) {
        try {
            $client = new Client();
            $apiPath = PDFConfig::get_string('dtrans', 'path_api', '/dtrans/v1/documents/share/get?key=%key%');
            $url = $frontend_uri . str_replace('%key%', $view_key, $apiPath);
            $response = $client->request('GET', $url);
            if($response->getStatusCode() != 200)
                throw new \HttpException('XML could not be downloaded. Server returned ' . $response->getStatusCode());
            if(strpos(strtolower(trim($response->getHeaderLine('content-type'))), 'text/xml') ||
                strpos(strtolower(trim($response->getHeaderLine('content-type'))), 'application/xml'))
                throw new \HttpException('XML could not be downloaded. Server returned invalid content type. ' . $response->getHeaderLine('content-type'));
            return $response->getBody();
        } catch (GuzzleException|\Exception $e) {
            PDFLogger::error('Failed to download pdf.', ['exception' => $e->__toString()]);
            return false;
        }
    }
}