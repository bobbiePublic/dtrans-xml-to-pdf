<?php

namespace DtransXML2PDF;

require_once __DIR__ . '/../config.php';

use Atgp\FacturX\Fpdi\FdpiFacturx;
use HeadlessChromium\BrowserFactory;
use HeadlessChromium\Page;
use setasign\Fpdi\PdfParser\StreamReader;

class PDFBuilder {
    // input
    protected string $view_key;
    protected ?string $lang;
    protected string $frontend_uri;
    protected bool $embed_xml;

    protected ?string $pdf = NULL;

    // for hybrid pdf xml attributes
    protected ?string $attachment_xml = NULL;
    protected ?string $attachment_filename = NULL;
    protected ?string $attachment_description = NULL;

    public function __construct(string $view_key, string $frontend_uri, ?string $lang = null, bool $embed_xml_as_hybrid = true)
    {
        $this->view_key = $view_key;
        $this->frontend_uri = $frontend_uri;
        $this->embed_xml = $embed_xml_as_hybrid;
        $this->lang = $lang;
        PDFLogger::info('Start rendering of document.', ['view_key' => $view_key, 'embed_xml' => $embed_xml_as_hybrid, 'lang' => $lang]);
    }

    private function render() : void {
        try {
            if(!is_file(PATH_TEMP_FILE))
                throw new \InvalidArgumentException('Socket file was not found');
            $socket = file_get_contents(PATH_TEMP_FILE);
            $browser = BrowserFactory::connectToBrowser($socket);
            PDFLogger::info('Loaded existing chrome instance.', ['chrome' => $socket]);
        } catch (\Exception $e) {
            $startupTimeout = PDFConfig::get_int('chromium', 'startup_timeout', 30);
            $chromiumUserDataDir = PDFConfig::get_string('chromium', 'userDataDir', NULL);
            if(empty($chromiumUserDataDir))
                $chromiumUserDataDir = false;
            $chromiumPath = PDFConfig::get_string('chromium', 'path', '/usr/bin/chromium');
            $browserFactory = new BrowserFactory($chromiumPath);
            $browserFactory->setOptions([
                'windowSize' => [1920, 1000],
                'enableImages' => true,
                'headless' => true,
                'startupTimeout' => $startupTimeout,
                'keepAlive' => true,
                'userDataDir' => $chromiumUserDataDir
            ]);

            $browser = $browserFactory->createBrowser();
            $socket = $browser->getSocketUri();
            file_put_contents(PATH_TEMP_FILE, $socket, LOCK_EX);
            PDFLogger::notice('Created new chrome instance.', ['chrome' => $socket]);
        }

        $renderPath = PDFConfig::get_string('dtrans', 'path_render', '/shared?doc=%key%');
        $url = $this->frontend_uri . str_replace('%key%', $this->view_key, $renderPath);
        if(!empty($this->lang))
            $url .= '&lang=' . $this->lang;

        try {
            $page = $browser->createPage();
            $renderTimeout = PDFConfig::get_int('pdf', 'render_timeout', 50 * 1000);
            $page->navigate($url)->waitForNavigation(Page::NETWORK_IDLE, $renderTimeout);
            $pdf = $page->pdf(['printBackground' => false])->getBase64();
            $this->pdf = base64_decode($pdf);
            $page->close();
        } catch (\Throwable|\Exception|\Error $e) {
            PDFLogger::error('Rendering PDF failed.', ['view_key' => $this->view_key, 'url' => $url, 'exception' => $e->__toString()]);
        }
    }

    public function create_pdf() /* :string|false */{
        if(is_null($this->pdf))
            $this->render();

        if($this->embed_xml) {
            $this->attachment_xml = PDFTools::download_xml($this->view_key, $this->frontend_uri);
            if($this->attachment_xml === false)
                return false;
            $this->attachment_filename = '1Lieferschein.xml';
            $this->attachment_description = 'Source ubl xml used to render this pdf.';
        }

        // rendering failed
        if(is_null($this->pdf))
            return false;

        // attach xml
        if(!is_null($this->attachment_xml)) {
            try {
                $pdfWriter = new FdpiFacturx();
                $pdfRef = StreamReader::createByString($this->pdf);
                $xmlRef = StreamReader::createByString($this->attachment_xml);
                $attachment_relationship = 'Source'; // The embarkation relationship, must be Data|Source|Alternative

                $pageCount = $pdfWriter->setSourceFile($pdfRef);
                for ($i = 1; $i <= $pageCount; $i++) {
                    $tplIdx = $pdfWriter->importPage($i, '/MediaBox');
                    $pdfWriter->AddPage();
                    $pdfWriter->useTemplate($tplIdx, 0, 0, null, null, true);
                }

                $pdfWriter->Attach($xmlRef, $this->attachment_filename, $this->attachment_description, $attachment_relationship, 'text#2Fxml');
                $pdfWriter->OpenAttachmentPane();
                $pdfWriter->SetPDFVersion('1.7', true); // version 1.7 according to PDF/A-3 ISO 32000-1
                $generatedFileName = '1Lieferschein-' . date('Ymdhis').'.pdf';
                PDFLogger::info('Generation of hybrid-pdf complete.', ['pdf_size' => $this->get_file_size()]);
                return $pdfWriter->Output($generatedFileName, 'S');
            } catch (\Exception $e) {
                PDFLogger::error('Attaching xml to pdf failed.', ['exception' => $e->__toString()]);
            }
        }

        PDFLogger::info('Generation of plain-pdf complete.', ['pdf_size' => $this->get_file_size()]);
        // return plain pdf (also fallback if hybrid pdf generation fails)
        return $this->pdf;
    }

    public function get_file_size() : int {
        if(is_null($this->pdf))
            return -1;

        if (function_exists('mb_strlen'))
            return  mb_strlen($this->pdf, '8bit');
        else
            return strlen($this->pdf);
    }
}