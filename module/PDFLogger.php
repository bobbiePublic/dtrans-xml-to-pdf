<?php

namespace DtransXML2PDF;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

abstract class PDFLogger {
    protected static Logger $logger;

    const LOGGING_FORMAT_LINE = "[%datetime%] (%channel%) %level_name%: %message% %context%\n"; // . '%extra\n%; // ignored because it is never used anywhere in dtrans
    const LOGGING_FORMAT_LINE_DATE = "Y-m-d\TH:i:s.uP";


    public static function _initialize(string $level = Logger::INFO) : void {
        // directory where log files are stored
        $log_dir = PATH_LOG_FILES;
        $formatter = new LineFormatter(self::LOGGING_FORMAT_LINE, self::LOGGING_FORMAT_LINE_DATE);

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $channel = '';
        for ($i = 0; $i < 8; $i++)
            $channel .= $characters[random_int(0, $charactersLength - 1)];

        // create the logger
        self::$logger = new Logger($channel);
        // full logger
        $maxFiles = 3;
        $handler = new RotatingFileHandler($log_dir . '/full-xml2pdf.log', $maxFiles, $level);
        $handler->setFormatter($formatter);
        self::$logger->pushHandler($handler);

        // error logger
        $handler = new StreamHandler($log_dir . '/error-xml2pdf.log', Logger::WARNING);
        $handler->setFormatter($formatter);
        self::$logger->pushHandler($handler);
    }

    public static function debug(string $message, array $extra = []) {
        if(empty($message) && empty($extra)) return;
        self::$logger->debug($message, $extra);
    }

    public static function info(string $message, array $extra = []) {
        if(empty($message) && empty($extra)) return;
        self::$logger->info($message, $extra);
    }

    public static function notice(string $message, array $extra = []) {
        if(empty($message) && empty($extra)) return;
        self::$logger->notice($message, $extra);
    }

    public static function warning(string $message, array $extra = []) {
        if(empty($message) && empty($extra)) return;
        self::$logger->warning($message, $extra);
    }

    public static function error(string $message, array $extra = []) {
        if(empty($message) && empty($extra)) return;
        self::$logger->error($message, $extra);
    }

    /* used for unexpected exceptions and crashes */
    public static function critical(string $message, array $extra = [])  {
        if(empty($message) && empty($extra)) return;
        self::$logger->critical($message , $extra);
    }
}
